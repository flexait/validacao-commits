#!/usr/bin/env node

const { execSync } = require("child_process");
const inquirer = require("inquirer");

const app = require("./index");
const log = require("./logger");

log.info("cz-customizable standalone version");

const commit = commitMessage => {
  try {
    execSync(`git commit -m "${commitMessage}"`, { stdio: [0, 1, 2] });
  } catch (error) {
    log.error(">>> ERROR", error.error);
  }
};

const commitBuildReact = commitMessage => {
  try {
    execSync(
      `sudo rm -rf production && mkdir production && react-scripts build && sudo mv build/* production/ && git add . && git commit -m "${commitMessage}"`,
      { stdio: [0, 1, 2] }
    );
  } catch (error) {
    log.error(">>> ERROR", error);
  }
};

app.prompter(inquirer, commit, commitBuildReact);
